<?php

define('secureConstantChecking', TRUE);

$array_of_actions = [
    'display_bottom_cookies',
    'display_popup_cookies',
    'display_cookie_settings',
    'display_cookie_about',
    'cookies_disabled_notice',
    'dnt_enabled_notice',
    'display_cookidnt_enabled_noticee_settings_button',
    'display_cookie_notice_regular',
    'load_tracking_scripts',
    'load_secured_cookies',
    'display_pp_notice',
    'returncookiename',
    'display_cookie_settings_button'
];

$action = $_POST['action'];
if (!empty($_POST['gdprlanguage'])) {
    $langGDPR = filter_var($_POST['gdprlanguage'], FILTER_SANITIZE_STRING);
} else {
    $langGDPR = false;
}

if ($action) {
    if (in_array($action, $array_of_actions)) {
        include 'db.php';
        require_once 'new-test-file.php';
        if ($langGDPR) {
            echo $action();
        } else {
            echo $action();
        }
    } else {
        echo 'Suspicious activity recorded';
        exit();
    }

}