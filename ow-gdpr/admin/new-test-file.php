<?php

if (!defined('secureConstantChecking')) exit;

global $langGDPR;
if ($langGDPR == 'zh') {
    $langGDPR = 'zh-hans';
}
$values = getGDPRValues($langGDPR);

// custom check function if the key and val are inside the array we're given :P
function checkIfCookieExistsInArray($array, $key, $val)
{
    if (isset($array[$key]) && $array[$key] == $val) {
        return true;
    } else {
        return false;
    }
}

//this function displays the html for the cookie notices. Content is text displayed in the notice bar, display settings is true or false, true to add the settings popup
function display_bottom_cookies($cont, $displaySettings = false, $displaydisablecookies = false, $forcereload = true)
{
    global $values;
    $output = '';

    if ($forcereload == true) {
        $output .= '<div class="forcereload"></div>';
    }
    $output .= '<div class="bottomfooter">';
    $output .= '<div class="cookies_container">';
    $output .= $cont;
    if ($displaydisablecookies == true) {
        $output .= '<div class="popuplinks aboutcookieslink">';
        $output .= '<div class="opencookiesettings">' . $values["button_text_cookie_settings"] . '</div>';
        $output .= '<div class="closecpopup acceptcookies ' . (($values["button_style_continue_browsing"] == "button") ? "makethisabutton" : "") . '">' . $values["continue_browsing_text"] . '</div>';
        $output .= '</div>';
    }
    $output .= '</div>';
    $output .= '<div class="closenotice acceptcookies"></div>';
    $output .= '</div>';
    if ($displaySettings == true) {
        $output .= display_cookie_settings();
        $output .= display_cookie_about();
    }

    return $output;
}

//displays notice for cookies in popup. This should only load for first load with do not track enabled
function display_popup_cookies($cont)
{
    global $values;
    //var_dump($values);
    $output = '';

    // Anže in Jaša sta želela odstraniti prvo stran - če jo želiš dobiti nazaj odstrani hidenotice class pod temule commentarjem
    // in pri spodnji funkciji display_cookie_settings daj oba argumenta kot false
    $output .= '<div class="popupcookies hidenotice">';
    $output .= '<div class="innerpopupcookies">';
    $output .= '<div class="cookies_container">';
    $output .= $cont;
    $output .= '<div class="popuplinks">';
    $output .= '<div class="opencookiesettings">' . $values["button_text_cookie_settings"] . '</div>';
    $output .= '<div class="closecpopup acceptcookies closecsettingstext cta-cookie-button  ' . (($values["button_style_continue_browsing"] == "button") ? "makethisabutton" : "") . '">' . $values["button_accept_cookies_text_explicit_consent"] . '</div>';
    $output .= '</div>';
    $output .= '</div>';
    //$output .= '<div class="closenotice"></div>';
    $output .= '</div>';
    $output .= '</div>';
    $output .= display_cookie_settings(false, true);
    $output .= display_cookie_about();
    return $output;
}

//this function displays settings popup + button; displaybutton parameters set visibility of buton on load
function display_cookie_settings($displaybutton = false, $primarydisplay = false)
{
    global $values;
    $output = '';

    //$output .= '<div class="opencookiesettings csettingsbutton '.(($displaybutton == true)?"":"hiddenbutton").'"></div>';
    $output .= '<div class="opencookiesettings csettingsbutton ' . (($displaybutton == true) ? "" : "hiddenbutton") . '"><span class="cookiesettext"></span></div>';
    $output .= '<div class="popupcookiessettings ' . (($primarydisplay == true) ? "" : "hidenotice") . '">';
    $output .= '<div class="innerpopupcookies">';
    $output .= '<div class="cookies_container">';
    // Start adding logic for tracking_permission
    if (isset($_COOKIE['tracking_permission'])) {
        $cookiestoload = $_COOKIE['tracking_permission'];
    } else {
        $cookiestoload = false;
    }
    if (strpos($cookiestoload, "script") !== false) {
        $classforsselection = "show";
        $checkedsomescripts = "checked";
        $exploded = explode("|", $cookiestoload);
    } else {
        $classforsselection = "";
        $checkedsomescripts = "";
        $exploded = "";
    }
    // end logic for tracking_permission

    // start logic for custom cookie names
    if (is_array($values['custom_cookie_name'])) {
        $multipleCookiesToLoad = array();
        foreach ($values['custom_cookie_name'] as $singleCookie) {
            if (isset($_COOKIE[$singleCookie['custom_cookie_value_name']])) {
                $multipleCookiesToLoad[$singleCookie['custom_cookie_value_name']] = $_COOKIE[$singleCookie['custom_cookie_value_name']];
            } else {

            }
        }
    }


    // end logic for custom cookie names
    $output .= $values["cookie_consent_in_settings_popup_text"];
    $output .= '<div class="cookiesettingscheckboxes">';
    $output .= '<label class="csettingsradio"><input type="radio" name="generalcookiesettings" ' . (($cookiestoload == 'all') ? "checked" : "") . (($cookiestoload == false) ? "checked" : "") . ' value="all"><span>' . $values["allow_all_cookies_text"] . '</span></label>';
    $output .= '<label class="csettingsradio"><input type="radio" name="generalcookiesettings" ' . $checkedsomescripts . ' value="justsome"><span>' . $values["allow_some_cookies_text"] . '</span></label>';
    $output .= '<div class="justsomecookies ' . $classforsselection . '">';
    $output .= '<div class="choosesome">' . $values["choose_cookies_to_allow_text"] . '</div>';
    if ($values['check_script_upload_manager'] == 'gtm' || $values['check_script_upload_manager'] == 'both') {
        $output .= '<label class="csettingscb"><input type="checkbox" checked disabled><span class="slider"></span><span class="tooltip-ow right-ow">' . $values["necessary_cookies_name"] . '
                        <span class="tooltip-content">' . $values["necessary_cookies_description"] . '</span>
                    </span></label>';
    }

    // Adding GTM scripts
    if ($values['check_script_upload_manager'] == 'gtm' || $values['check_script_upload_manager'] == 'both') {
        $output .= '<label class="csettingscb"><input type="checkbox" ' . (($exploded == '' || in_array('analytics', $exploded)) ? "checked" : "") . ' name="selectedcookies" value="analytics" class="ow-custom-cookie-input"><span class="slider"></span><span class="tooltip-ow right-ow">' . $values["analytical_cookies_name"] . '
                    <span class="tooltip-content">' . $values["analytical_cookies_description"] . '</span>
                </span></label>';
        $output .= '<label class="csettingscb"><input type="checkbox" ' . (($exploded == '' || in_array('marketing', $exploded)) ? "checked" : "") . ' name="selectedcookies" value="marketing" class="ow-custom-cookie-input"><span class="slider"></span><span class="tooltip-ow right-ow">' . $values["marketing_cookies_name"] . '
                    <span class="tooltip-content">' . $values["marketing_cookies_description"] . '</span>
                </span></label>';
    }
    $countscript = 1;
    if ($values['check_script_upload_manager'] == 'scripts' || $values['check_script_upload_manager'] == 'both') {
        foreach ($values["tracking_scripts_to_load"] as $script) {
            $output .= '<label class="csettingscb"><input type="checkbox" ' . (($exploded == '' || in_array($countscript, $exploded)) ? "checked" : "") . ' name="selectedcookies" value="' . $countscript . '"><span class="slider"></span><span class="tooltip-ow tooltip-no-questionmark">' . $script["script_name"] . '</span></label>';
            $countscript++;
        }
    }
    // custom cookie add checked or dont add it :P
    if (is_array($values['custom_cookie_name']) && $values['check_for_cookie_name'][0] == 'Yes') {
        foreach ($values['custom_cookie_name'] as $script) {
            if (checkIfCookieExistsInArray($multipleCookiesToLoad, $script['custom_cookie_value_name'], $script['custom_cookie_value']) || (empty($multipleCookiesToLoad))) {
                $output .= '<label class="csettingscb"><input type="checkbox" checked name="' . $script['custom_cookie_value_name'] . '" value="' . $script["custom_cookie_value"] . '" class="ow-custom-cookie-input"><span class="slider"></span><span class="tooltip-ow tooltip-no-questionmark">' . $script["custom_cookie_front_name"] . '</span></label>';
            } else {
                $output .= '<label class="csettingscb"><input type="checkbox" name="' . $script['custom_cookie_value_name'] . '" value="' . $script["custom_cookie_value"] . '" class="ow-custom-cookie-input"><span class="slider"></span><span class="tooltip-ow tooltip-no-questionmark">' . $script["custom_cookie_front_name"] . '</span></label>';
            }
        }
    }


    //youtube in vimeo piškot zaenkrat ni vklopljen, se vklaplja po potrebi. Prav tako je zakomentirano v ow_cookie_consent.js
    //$output .= '<label class="csettingscb"><input type="checkbox" '.(($exploded == '' || in_array("v", $exploded))?"checked":"").' name="selectedcookies" value="v"><span>YouTube & Vimeo</span></label>';
    $output .= '</div>';
    $output .= '<label class="csettingsradio"><input type="radio" name="generalcookiesettings" value="none" ' . (($cookiestoload == 'none') ? "checked" : "") . '><span>' . $values["allow_no_cookies_text"] . '</span></label>';

    $output .= '</div>';
    $output .= '<div class="closecsettingstext ' . (($values["button_style_continue_browsing"] == "button") ? "makethisabutton" : "") . '">' . $values["save_settings_button_text"] . '</div>';
    $output .= '</div>';
    //$output .= '<div class="closenotice"></div>';
    $output .= '</div>';
    $output .= '</div>';
    return $output;
}

//popup of we use cookies text
function display_cookie_about()
{
    global $values;
    $output = '';

    $output .= '<div class="popupcookiesabout hidenotice">';
    $output .= '<div class="innerpopupcookies">';
    $output .= '<div class="cookies_container">';
    $output .= $values["cookies_privacy_policy_text"];
    $output .= $values["our_cookies_text"];
    $output .= '</div>';
    $output .= '<div class="popuplinks aboutcookieslink">';
    $output .= '<div class="opencookiesettings">' . $values["button_text_cookie_settings"] . '</div>';
    //$output .= '<div class="closecpopup '.(($values["button_style_continue_browsing"] == "button")?"makethisabutton":"").'">'.$values["button_text_continue_browsing"].'</div>';
    $output .= '</div>';
    //$output .= '<div class="closenotice"></div>';
    $output .= '</div>';
    $output .= '</div>';
    return $output;
}

//this function dsiplays output when cookies are disabled on browser
function cookies_disabled_notice()
{
    global $values;

    $cont = $values["cookies_disabled_notice"];
    $html = display_bottom_cookies($cont);
    $html .= "console.log('Cookies bottom cookies')";
    echo $html;
    return $html;
    //exit();
}

//this function dsiplays output when cookies are enabled, and Do not track is set on browser. Displays notice + settings
function DNT_enabled_notice()
{
    global $values;
    $cont = $values["hard_consent_for_no_tracking_browsers_text"];
    $html = display_cookie_notice_regular($cont);
    echo $html;
    exit();
}

//this function dsiplays output when cookies are enabled, and Do not track is set on browser. Displays notice + settings
function display_cookie_settings_button($lang = '')
{
    $html = display_cookie_settings(true);
    $html .= display_cookie_about();
    echo $html;
    exit();
}

//this function dsiplays output when cookies are enabled, and Do not track is NOT set on browser. Displays notice + settings
function display_cookie_notice_regular()
{
    global $values;

    if ($values["how_to_treat_non_DNT"] == 'explicit consent') {
        $cont = $values["cookies_consent_text"];
        $html = display_popup_cookies($cont, true);
    } else {
        $html = display_bottom_cookies($values["cookies_consent_text"], true, true, true);
    }
    //$html .= display_cookie_settings();
    echo $html;
    exit();
}

//this function dsiplays output when cookies are enabled, and Do not track is set on browser. Displays notice + settings
function load_tracking_scripts()
{
    global $values;
    $html = [];

    $cookiestoload = $_COOKIE['tracking_permission'];
    if (strpos($cookiestoload, "script") !== false) {
        $exploded = explode("|", $cookiestoload);
    }


    //if($cookiestoload == 'none'){$html = 'LMAO';}
    $countscript = 1;
    foreach ($values["tracking_scripts_to_load"] as $script) {
        if ($cookiestoload == 'all' || in_array($countscript, $exploded)) {
            if ($values['check_script_upload_manager'] == 'scripts' || $values['check_script_upload_manager'] == 'both') {
                if ($script['script_code_head'][0] != 'Yes') {
                    $html['body'] .= $script["script_code"];
                }
            }
        }
        $countscript++;
    }
    if ($values['check_script_upload_manager'] == 'gtm' || $values['check_script_upload_manager'] == 'both') {
        foreach ($values["tracking_scripts_gtm"] as $script) {
            $html['head'] .= $script["head_script_code"];
            $html['body'] .= $script["body_script_code"];
        }
    }
    $countscript = 1;
    foreach ($values["tracking_scripts_to_load"] as $script) {
        if ($cookiestoload == 'all') {
            if ($values['check_script_upload_manager'] == 'scripts' || $values['check_script_upload_manager'] == 'both') {
                if (!empty($script['script_code_head'])) {
                    if ($script['script_code_head'][0] == 'Yes') {
                        $html['head'] .= $script["script_code"];
                    }
                }
            }
        }
        if (is_array($exploded)) {
            if (in_array($countscript, $exploded)) {
                if ($values['check_script_upload_manager'] == 'scripts' || $values['check_script_upload_manager'] == 'both') {
                    if (!empty($script['script_code_head'])) {
                        if ($script['script_code_head'][0] == 'Yes') {
                            $html['head'] .= $script["script_code"];
                        }
                    }
                }
            }
        }
        $countscript++;
    }

    $html = json_encode($html, JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_LINE_TERMINATORS);
    echo $html;
    exit();
}

function load_secured_cookies()
{
    global $values;

    if ($values['are_cookies_secured'] == 'secure') {
        $html = 'SameSite=None; secure;';
    } else {
        $html = '';
    }

    echo $html;
    exit();
}


/**
 * Privacy Settings
 */

function display_pp_notice()
{
    global $values;
    $convertedObject = get_object_vars($values['pppage']);
    $cont = $values["ppnoticetext"];
    $html = "<div class='ppnoticebar'><div class='container'>" . $cont . "<a href='" . $convertedObject['guid'] . "' class='ppolicylink'>" . $values['privacy_link_text'] . "</a><div class='closeprivacy'>X</div></div></div>";
    echo $html;
    exit();
}

function returncookiename()
{
    global $values;
    // $settingspage=  apply_filters( 'wpml_object_id', 184, 'page', true );
    $name = $values["pp_cookie_name"];
    echo $name;
    exit();
}