<?php

if (!defined('ABSPATH')) exit;

//enqueue scripts + add container to load the html elements into to footer
function ow_cookie_consent()
{
    echo '<script type="text/javascript">var test; var ajaxurl="' . plugin_dir_url(__FILE__) . 'faster-custom-ajax.php"</script>';
    if (get_field("enable_cookies_settings", "option") == "yes") {
        $checkForAdminVisibility = get_field("visible_to_admin", "option");
        echo '<script src="' . get_site_url() . '/wp-content/plugins/ow-gdpr/js/ow_cookie_consent.js"></script>';
        if (isset($checkForAdminVisibility[0])) {
            if ($checkForAdminVisibility[0] == "Yes" && !acf_current_user_can_admin()) {
                echo '<div id="ow_cookie_consent_container" class="hide-for-visitors-only"></div>';
            } else {
                echo '<div id="ow_cookie_consent_container"></div>';
            }
        } else {
            echo '<div id="ow_cookie_consent_container"></div>';
        }
        echo '<div id="trackingscripts"></div>';
    }
    if (get_field("enable_privacy_policy", "option") == "yes") {
        echo '<script src="' . get_site_url() . '/wp-content/plugins/ow-gdpr/js/ppscript.js"></script>';
    }
}

add_action('wp_footer', 'ow_cookie_consent');


function add_gtm_into_head()
{
    $currentActiveLanguage = get_locale();
    $currentActiveLanguage = explode("_", $currentActiveLanguage);
    $currentActiveLanguage = $currentActiveLanguage[0];
    if ($currentActiveLanguage == 'zh') {
        $currentActiveLanguage = 'zh-hans';
    }
    /*$values = get_option('ow_gdpr_info_'.$currentActiveLanguage);
    if($values['check_script_upload_manager'] == 'gtm' || $values['check_script_upload_manager'] == 'both'){
        foreach($values["tracking_scripts_gtm"] as $script){
            echo $script["head_script_code"];
        }
    }*/
    if (get_field("enable_cookies_settings", "option") == "yes") {
        echo '<script id="trackingheadscripts"></script>';
    }

}

add_action('wp_head', 'add_gtm_into_head');

function pp_notice_html()
{
    $html = '<div class="privacynotice"></div>';
    if (get_field("enable_privacy_policy", "option") == "yes") {
        echo $html;
    }
}

add_action('wp_head', 'pp_notice_html');

function my_plugin_wpml_code()
{
    // Run WPML dependent actions
    return true;
}

add_action('wpml_loaded', 'my_plugin_wpml_code');

//spreminanje delovanja update funkcije
function my_acf_save_post($post_id)
{
    $screen = get_current_screen();
    $values = get_fields($post_id);
    $currentActiveLanguage = get_locale();
    var_dump($currentActiveLanguage);
    $currentActiveLanguage = explode("_", $currentActiveLanguage);
    $currentActiveLanguage = $currentActiveLanguage[0];
    if ($currentActiveLanguage == 'zh') {
        $currentActiveLanguage = 'zh-hans';
    }

    if (function_exists('icl_object_id')) {
        $row_value = 'ow_gdpr_info_' . ICL_LANGUAGE_CODE;
    } else {
        $row_value = 'ow_gdpr_info_' . $currentActiveLanguage;
    }

    $neccessary_values = array(
        'enable_cookies_settings' => $values['enable_cookies_settings'],
        'allow_all_cookies_text' => $values['allow_all_cookies_text'],
        'allow_no_cookies_text' => $values['allow_no_cookies_text'],
        'allow_some_cookies_text' => $values['allow_some_cookies_text'],
        'analytical_cookies_description' => $values['analytical_cookies_description'],
        'analytical_cookies_name' => $values['analytical_cookies_name'],
        'are_cookies_secured' => $values['are_cookies_secured'],
        'button_accept_cookies_text_explicit_consent' => $values['button_accept_cookies_text_explicit_consent'],
        'button_style_continue_browsing' => $values['button_style_continue_browsing'],
        'button_text_continue_browsing' => $values['button_text_continue_browsing'],
        'button_text_cookie_settings' => $values['button_text_cookie_settings'],
        'check_for_cookie_name' => $values['check_for_cookie_name'],
        'check_script_upload_manager' => $values['check_script_upload_manager'],
        'choose_cookies_to_allow_text' => $values['choose_cookies_to_allow_text'],
        'continue_browsing_text' => $values['continue_browsing_text'],
        'cookie_consent_in_settings_popup_text' => $values['cookie_consent_in_settings_popup_text'],
        'cookie_settings_button_text_floating_cookie' => $values['cookie_settings_button_text_floating_cookie'],
        'cookies_consent_text' => $values['cookies_consent_text'],
        'cookies_disabled_notice' => $values['cookies_disabled_notice'],
        'cookies_privacy_policy_text' => $values['cookies_privacy_policy_text'],
        'custom_cookie_name' => $values['custom_cookie_name'],
        'enable_cookies_settings' => $values['enable_cookies_settings'],
        'enable_privacy_policy' => $values['enable_privacy_policy'],
        'hard_consent_for_no_tracking_browsers_text' => $values['hard_consent_for_no_tracking_browsers_text'],
        'how_to_treat_non_DNT' => $values['how_to_treat_non_DNT'],
        'marketing_cookies_description' => $values['marketing_cookies_description'],
        'marketing_cookies_name' => $values['marketing_cookies_name'],
        'necessary_cookies_description' => $values['necessary_cookies_description'],
        'necessary_cookies_name' => $values['necessary_cookies_name'],
        'our_cookies_text' => $values['our_cookies_text'],
        'pp_cookie_name' => $values['pp_cookie_name'],
        'ppnoticetext' => $values['ppnoticetext'],
        'pppage' => $values['pppage'],
        'privacy_link_text' => $values['privacy_link_text'],
        'save_settings_button_text' => $values['save_settings_button_text'],
        //'tracking_scripts_gtm' => $values['tracking_scripts_gtm'],
        'tracking_scripts_gtm' => array($values['tracking_scripts_gtm'][0]),
        'tracking_scripts_to_load' => $values['tracking_scripts_to_load'],
        'lmao' => 'okok'
    );

    /*  echo '<pre>';
    var_dump($neccessary_values['tracking_scripts_gtm']);
    echo '<br><br><br>';
    var_dump($values['tracking_scripts_gtm']);
    echo '<br>';
    var_dump(update_option($row_value, $neccessary_values));
    echo '</pre>';
    exit();  */
    if ((strpos($screen->id, "gdpr-settings") == true) || (strpos($screen->id, "acf-options-cookie-settings") == true) || (strpos($screen->id, "acf-options-privacy-policy") == true)) {
        //$values = json_encode($values);
        $neccessary_values = json_encode($neccessary_values);
        update_option($row_value, $neccessary_values);
    }
}

add_action('acf/save_post', 'my_acf_save_post', 15);


$viewed_cookie = "pys_cookie";
$out_fn = '__return_true'; //block it
$out = true;
if (isset($_COOKIE[$viewed_cookie])) {
    if ($_COOKIE[$viewed_cookie] == 'true') {
        $out_fn = '__return_false'; //remove blocking
        $out = false;
    }
}
if (is_plugin_active_gdpr()) {
    add_filter('pys_disable_by_gdpr', $out_fn, 10, 2);
}


function is_plugin_active_gdpr()
{
    if (!function_exists('is_plugin_active')) {
        include_once(ABSPATH . 'wp-admin/includes/plugin.php');
    }
    return is_plugin_active('pixelyoursite-pro/pixelyoursite-pro.php') || is_plugin_active('pixelyoursite/facebook-pixel-master.php');
}