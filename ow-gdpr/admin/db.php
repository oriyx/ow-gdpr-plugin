<?php

if (!defined('secureConstantChecking')) exit;

function getWantedConstants()
{
    # Extract thoes constants
    $wantedConfig = array(
        "DB_NAME", "DB_USER", "DB_PASSWORD", "DB_HOST"
    );

    # Get Config file
    $config = file_get_contents("../../../../wp-config.php");


    # Match capture groups
    preg_match_all("/define\(\s*?['|\"]([A-Z_]+)['|\"]\s*?,\s*['|\"](.*)'\s*?\);/", $config, $matches);

    # Capture Group #2
    $variables = $matches[1];

    # Capture Group #3
    $values = $matches[2];

    # Combine capture groups into All constants
    $allConstants = array_combine($variables, $values);

    # Extract wanted constants using Config
    $wantedConstants = array_filter($allConstants, function ($key) use ($wantedConfig) {
        return in_array($key, $wantedConfig);
    }, ARRAY_FILTER_USE_KEY);

    return $wantedConstants;
}


function getDatabasePrefix()
{
    # What we're looking for
    $wantedPrefix = array(
        "table_prefix",
    );

    # Get Config file
    $config = file_get_contents("../../../../wp-config.php");

    # Match capture groups
    preg_match_all("/table_prefix\s+=\s*?['\"]([a-z_]+)['\"]\;/", $config, $matches);

    # Match 1
    $value = $matches[1][0];
    return $value;


}

function getGDPRValues($langGDPR = false)
{
    $constants = getWantedConstants();
    $prefix = getDatabasePrefix();
    $con = mysqli_connect($constants["DB_HOST"], $constants["DB_USER"], $constants["DB_PASSWORD"], $constants["DB_NAME"]);
    $con->set_charset('utf8mb4');
    $tableName = $prefix . 'options';

    if ($langGDPR) {
        $optionName = 'ow_gdpr_info_' . $langGDPR;
        $sql = "SELECT * FROM " . $tableName . " WHERE option_name='" . $optionName . "'";
    } else {
        $sql = "SELECT * FROM " . $tableName . " WHERE option_name='ow_gdpr_info_en'";
    }
    $query = $con->query($sql);
    //todo - sanitize, escapestring, pdo, config file za urejanje jezikov

    $result = $query->fetch_assoc();
    $value = json_decode($result["option_value"], TRUE);
    mysqli_close($con);

    return $value;
}