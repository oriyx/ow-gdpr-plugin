<?php

//start privacy policy
add_action('init', 'create_post_type_ppolicy');
function create_post_type_ppolicy()
{
    $labels = array(
        'name' => _x('Privacy policy', 'post type general name'),
        'singular_name' => _x('Privacy policy', 'post type singular name'),
        'add_new' => _x('Add new Privacy policy', 'book'),
        'add_new_item' => __('Add new Privacy policy'),
        'edit_item' => __('Edit'),
        'new_item' => __('Add new Privacy policy'),
        'all_items' => __('All Privacy policy'),
        'view_item' => __('View'),
        'search_items' => __('Search'),
        'not_found' => __('Iskanje ni našlo rezultatov'),
        'not_found_in_trash' => __('Privacy policy ni v košu'),
        'parent_item_colon' => '',
        'menu_name' => 'Privacy policy'

    );
    $args = array(
        'labels' => $labels,
        'public' => true,
        'publicly_queryable' => true,
        'show_ui' => true,
        'show_in_menu' => true,
        'query_var' => true,
        'capability_type' => 'post',
        'has_archive' => false,

        'menu_position' => 8,
        'supports' => array('title', 'author', 'editor', 'thumbnail', 'page-attributes')
    );
    register_post_type('privacy-policies', $args);
}    //end custom post type privacy policy

function display_ppolicy_dd()
{
    $args = array(
        'post_type' => 'privacy-policies',
        'post_status' => 'publish',
        'order' => 'DESC',
    );
    $currentid = get_the_ID();
    $query = new WP_Query($args);
    if ($query->have_posts()) {
        $html = '<div class="ppddwrap">';
        $html .= '<select id="selectppolicy">';
        $count = 1;
        while ($query->have_posts()) {
            $query->the_post();
            {
                if ($count == 1) {
                    $link = get_the_permalink(apply_filters('wpml_object_id', 20183, 'page', true));
                } else {
                    $link = get_the_permalink();
                }
                $html .= '<option link="' . $link . '" ' . (($currentid == get_the_ID()) ? "selected" : "") . '>' . get_the_title() . (($count == 1) ? " - latest" : "") . '</option>';
                $count++;
            }
        }
        $html .= '</select>';
        $html .= '</div>';
    }
    wp_reset_query();
    return $html;
}

add_shortcode("ppdd", "display_ppolicy_dd");

//add_action('init', 'create_post_type_partner');

function display_latestppcontent()
{
    $args = array(
        'post_type' => 'privacy-policies',
        'post_status' => 'publish',
        'order' => 'DESC',
        'posts_per_page' => 1
    );

    $query = new WP_Query($args);
    if ($query->have_posts()) {

        while ($query->have_posts()) {
            $query->the_post();
            {
                $html .= '<div class="legal_notes_wrapper">';
                $html .= apply_filters('the_content', get_the_content());
                $html .= '</div>';

            }
        }

    }
    wp_reset_query();
    return $html;
}

add_shortcode("latestppcontent", "display_latestppcontent");


?>