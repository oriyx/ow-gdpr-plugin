<?php

if (!defined('ABSPATH')) exit;
if (function_exists('acf_add_options_page')) {
    acf_add_options_page(array(
        'page_title' => 'GDPR settings',
        'menu_title' => 'GDPR settings',
        'menu_slug' => 'gdpr-settings',
        'capability' => 'edit_posts',
        'redirect' => false
    ));

    if (class_exists('SitePress')) {
        global $sitepress;
        $def_lang = $sitepress->get_default_language();
        $LANG = ICL_LANGUAGE_CODE == $def_lang ? '' : ICL_LANGUAGE_CODE . '_';
    } else {
        $LANG = '';
    }

    if (get_field($LANG . "enable_cookies_settings", "option") == 'yes') {
        acf_add_options_sub_page(array("menu_title" => "Cookie settings", "parent_slug" => "gdpr-settings"));
    }
    if (get_field("enable_privacy_policy", "option") == 'yes') {
        acf_add_options_sub_page(array("menu_title" => "Privacy policy", "parent_slug" => "gdpr-settings"));
    }

}


if (function_exists('acf_add_local_field_group')):

    acf_add_local_field_group(array(
        'key' => 'group_5af3ef4b7f76a',
        'title' => 'Cookies settings',
        'fields' => array(
            array(
                'key' => 'field_5af3efrh12346',
                'label' => 'Visible only to admins',
                'name' => 'visible_to_admin',
                'type' => 'checkbox',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array(
                    'Yes' => 'Visible only to admin'
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af3efrh67f3p',
                'label' => 'Do you wish the cookies to be secured?',
                'name' => 'are_cookies_secured',
                'type' => 'select',
                'choices' => array(
                    'secure' => 'Secure',
                    'notsecured' => 'Not Secure',
                ),
                'instructions' => 'If you choose secured - the cookie only works with HTTPS connection.',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af3ef64345fr',
                'label' => 'What type of script manager do you have?',
                'name' => 'check_script_upload_manager',
                'type' => 'select',
                'choices' => array(
                    'None' => 'Select',
                    'gtm' => 'Google Tag Manager',
                    'scripts' => 'Separate Script Upload',
                    'both' => 'Both types'
                ),
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af3ef3afc6d2',
                'label' => 'Tracking scripts to load',
                'name' => 'tracking_scripts_to_load',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'scripts'
                        )
                    ),
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'both'
                        )
                    )
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 0,
                'max' => 0,
                'layout' => 'table',
                'button_label' => '',
                'sub_fields' => array(
                    array(
                        'key' => 'field_5af3ef50fc6d3',
                        'label' => 'Script name',
                        'name' => 'script_name',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5af3ef64fc6d4',
                        'label' => 'Script code',
                        'name' => 'script_code',
                        'type' => 'textarea',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5af3ef64fc6o4',
                        'label' => 'Script code Head',
                        'name' => 'script_code_head',
                        'type' => 'checkbox',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'choices' => array(
                            'Yes' => 'Yes I want the script in the head'
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                ),
            ),
            array(
                'key' => 'field_5af3ef3aet6t3',
                'label' => 'Tracking scripts for Google Tag Manager',
                'name' => 'tracking_scripts_gtm',
                'type' => 'repeater',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'gtm'
                        )
                    ),
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'both'
                        )
                    )
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'collapsed' => '',
                'min' => 0,
                'max' => 0,
                'layout' => 'table',
                'button_label' => '',
                'sub_fields' => array(
                    array(
                        'key' => 'field_5af3ef64fc333',
                        'label' => 'Head Script Code',
                        'name' => 'head_script_code',
                        'type' => 'textarea',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5af3ef64fc555',
                        'label' => 'Body Script Code',
                        'name' => 'body_script_code',
                        'type' => 'textarea',
                        'instructions' => '',
                        'required' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                ),
            ),
            array(
                'key' => 'field_5af3ef64fd534',
                'label' => 'Obvezni piškotki',
                'name' => 'necessary_cookies_name',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'gtm'
                        )
                    ),
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'both'
                        )
                    )
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Nujni',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af3ef64fd535',
                'label' => 'Opis Obvezni Piškotki',
                'name' => 'necessary_cookies_description',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'gtm'
                        )
                    ),
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'both'
                        )
                    )
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Te piškotki so obvezni za delovanje spletne strani in jih ni mogoče izklučiti v naših sistemih. Običajno so le nastavljeni kot odziv na storitve, ki jih uporabljate na strani, kot so prijava, izpolnjevanje form in nastavitev zasebnosti. Lahko nastavitev svoj brskalnik da blokira oziroma vas obvesti o teh piškotkih, kar lahko povzroči nedelovanje nekaterih stvari na strani. Te piškotki ne hranijo osebnih informacij.',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af3ef64fe419',
                'label' => 'Analitični Piškotki',
                'name' => 'analytical_cookies_name',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'gtm'
                        )
                    ),
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'both'
                        )
                    )
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Analitični',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af3ef64f3r345',
                'label' => 'Opis Analitični Piškotki',
                'name' => 'analytical_cookies_description',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'gtm'
                        )
                    ),
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'both'
                        )
                    )
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Te piškotki nam dovolijo, da štejemo obisk in izvor spletnega prometa ter tako merimo in izboljšamo stanje spletne strani. Pomagajo nam ugotoviti katere strani so najbolj in najmanj popularne in kako se obiskovalci premikajo po strani. Vse pridobljene informacije teh piškotkov so zbrane kot skupen in tako tudi anonimne. Če ne dovolite teh piškotkov nebomo vedeli če ste obiskali našo stran.',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af3ef64f54k7',
                'label' => 'Marketing Piškotki',
                'name' => 'marketing_cookies_name',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'gtm'
                        )
                    ),
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'both'
                        )
                    )
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Oglaševalski',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af3ef64sg5432',
                'label' => 'Opis Marketing Piškotki',
                'name' => 'marketing_cookies_description',
                'type' => 'textarea',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'gtm'
                        )
                    ),
                    array(
                        array(
                            'field' => 'field_5af3ef64345fr',
                            'operator' => '==',
                            'value' => 'both'
                        )
                    )
                ),
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Te piškotki so lahko nastavljeni po naši strani iz strani naših oglaševalskih partnerjev. Lahko so uporabljene iz strani teh podjetji, da gradijo profil tvojih interesov in vam pokažejo relativne oglase na drugih straneh. Ne hranijo osebnih informacij vendar so bazirani na unikatnem prepoznavanjem vašega brskalnika in internetne naprave. Če ne dovolite teh piškotkov boste imeli manj tarčnega oglaševalstva.',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af3ef64ff395',
                'label' => 'Do you want a custom name for a cookie?',
                'name' => 'check_for_cookie_name',
                'type' => 'checkbox',
                'choices' => array(
                    'Yes' => 'Yes I want a custom cookie name'
                ),
                'instructions' => "If you're using pixelyoursite - you need this feature!",
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af3ef64fgda',
                'label' => 'Custom Cookie Names',
                'name' => 'custom_cookie_name',
                'type' => 'repeater',
                'conditional_logic' => array(
                    array(
                        array(
                            'field' => 'field_5af3ef64ff395',
                            'operator' => '==',
                            'value' => 'Yes'
                        )
                    )
                ),
                'instructions' => 'For PixelYourSite create following:<br>Fronend name : Pixel<br>Cookie name : pys_cookie<br>Cookie value : true',
                'required' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
                'sub_fields' => array(
                    array(
                        'key' => 'field_5af3ef503t8ko',
                        'label' => 'Frontend Cookie name',
                        'name' => 'custom_cookie_front_name',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5af3ef50fi4zz',
                        'label' => 'Cookie name',
                        'name' => 'custom_cookie_value_name',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                    array(
                        'key' => 'field_5af3ef64reg43',
                        'label' => 'Cookie value',
                        'name' => 'custom_cookie_value',
                        'type' => 'text',
                        'instructions' => '',
                        'required' => 0,
                        'conditional_logic' => 0,
                        'wrapper' => array(
                            'width' => '',
                            'class' => '',
                            'id' => '',
                        ),
                        'default_value' => '',
                        'placeholder' => '',
                        'prepend' => '',
                        'append' => '',
                        'maxlength' => '',
                    ),
                ),
            ),
            array(
                'key' => 'field_5af3ef79fc6d5',
                'label' => 'How to treat non DNT visitors (Choose Explicitni consent if you do not know which)',
                'name' => 'how_to_treat_non_DNT',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array(
                    'explicit consent' => 'explicit consent',
                    'soft consent' => 'soft consent',
                ),
                'default_value' => array(),
                'allow_null' => 0,
                'multiple' => 0,
                'ui' => 0,
                'ajax' => 0,
                'return_format' => 'value',
                'placeholder' => '',
            ),

            array(
                'key' => 'field_5af3ef9afc6d6',
                'label' => 'Cookies consent text - regular',
                'name' => 'cookies_consent_text',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Uporabljamo piškotke, ki nam s pomočjo statistik pomagajo, da bo tvoja izkušnja tukaj ena najboljših. Brez skrbi – so domači.
Če niso po tvojem okusu, jih lahko onemogočiš ali se o njih pozanimaš v <a href="#">politiki piškotkov</a>. Če pa z brskanjem po strani nadaljuješ brez spreminjanja nastavitev, bomo razumeli, da se z uporabo piškotkov v celoti strinjaš.',
                'tabs' => 'all',
                'toolbar' => 'full',
                'media_upload' => 1,
                'delay' => 0,
            ),
            array(
                'key' => 'field_5af3efc0fc6d7',
                'label' => 'Hard consent (for no tracking browsers) text',
                'name' => 'hard_consent_for_no_tracking_browsers_text',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '<strong>Za dostop do vseh funkcionalnosti strani potrdi uporabo piškotkov.</strong>
Brez piškotkov ti bo dostop do vseh funkcionalnosti strani, za katere smo se tako trudili, onemogočen.
Dovoljenje za vse ali posamezne piškotke lahko urediš v <a href="#">nastavitvah piškotkov</a>.',
                'tabs' => 'all',
                'toolbar' => 'full',
                'media_upload' => 1,
                'delay' => 0,
            ),
            array(
                'key' => 'field_5af3efdafc6d8',
                'label' => 'Cookie consent in settings popup text',
                'name' => 'cookie_consent_in_settings_popup_text',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '<h3>Obvestilo o piškotkih</h3>
Tudi mi uporabljamo piškotke, ki nam s  pomočjo statistik pomagajo, da bo tvoja izkušnja tukaj ena najboljših. A brez skrbi – so domači.
Več o tem si lahko prebereš <a href="#">tukaj.</a> Izklopiš ali vklopiš pa jih lahko spodaj.',
                'tabs' => 'all',
                'toolbar' => 'full',
                'media_upload' => 1,
                'delay' => 0,
            ),
            array(
                'key' => 'field_5af3eff5fc6d9',
                'label' => 'Cookies disabled notice',
                'name' => 'cookies_disabled_notice',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '<h3>Piškotkov ni!</h3>
Piškotke imaš izklopljene. Za najboljšo izkušnjo na spletni strani jih vklopi - brez skrbi, so domači! :)',
                'tabs' => 'all',
                'toolbar' => 'full',
                'media_upload' => 1,
                'delay' => 0,
            ),
            array(
                'key' => 'field_5af3ef9egv44q',
                'label' => 'Cookies privacy policy text',
                'name' => 'cookies_privacy_policy_text',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'tabs' => 'all',
                'toolbar' => 'full',
                'media_upload' => 1,
                'delay' => 0,
            ),
            array(
                'key' => 'field_5af3f000fc6da',
                'label' => 'Our cookies text',
                'name' => 'our_cookies_text',
                'type' => 'wysiwyg',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '<h3>Obvestilo o piškotkih</h3>
&nbsp;

<strong>O pogojih uporabe</strong>
S pogoji uporabe piškotkov vam ponujamo informacije o tem, kako uporabljamo piškotke in podobne tehnologije na naši spletni strani.

&nbsp;

<strong>Piškotki</strong>
Ko uporabljate našo spletno stran, na vašo napravo pošljemo enega ali več piškotkov – majhne tekstovne datoteke, ki vključujejo alfanumerične znake. Pri tem lahko uporabimo sejne piškotke ali trajne piškotke.
Sejni piškotki z vaše naprave izginejo takoj, ko zaprete okno brskalnika (ko se zaključi seja). Trajni piškotek pa v vaši napravi ostane tudi po tem, ko brskalnik zaprete, brskalnik pa ga lahko uporabi ob vseh naslednjih obiskih naše spletne strani. V povezavi z drugimi storitvami, ki so povezane z našo spletno stranjo, lahko uporabimo tudi piškotke tretjih oseb.

&nbsp;

<strong>Kako uporabljamo piškotke in podobne tehnologije</strong>
Piškotke in avtomatično zbrane podatke lahko uporabimo za:
(1) prirejanje naše spletne strani in drugih storitev, povezanih z našo spletno stranjo, kot je na primer pomnenje vaših informacij z namenom, da vam jih ob naslednjem obisku naše spletne strani ali drugih storitev, povezanih z našo spletno stranjo, ne bi bilo treba ponovno vnesti; (2) prilagajanje oglasov, vsebine in informacij; (3) spremljanje in analizo učinkovitosti našega spletnega mesta, drugih storitev, povezanih z našo spletno stranjo, in oglaševalskimi aktivnostmi; (4) spremljanje vseh metrik uporabe naše spletne strani, kot je na primer skupno število obiskovalcev in število obiskanih strani.

Tehnologija sledenja nam omogoča tudi urejanje in izboljšanje uporabnosti naše spletne strani, saj lahko z njo (1) zaznamo, če ste bili v preteklosti že v kontaktu z našo spletno stranjo in (2) prepoznamo najbolj priljubljene dele naše spletne strani.

&nbsp;

<strong>Kako lahko spremenim svoje nastavitve piškotkov?</strong>
Svojemu brskalniku z urejanjem nastavitev lahko naročite, da zavrne sprejemanje piškotkov katerekoli spletne strani, ki jo obiščete, s pomočjo ustrezne funkcije brskalnika pa lahko piškotke z vašega računalnika ali mobilne naprave tudi izbrišete.
Če piškotke onemogočite, lahko določeni deli in vsebine naše spletne strani in storitev postanejo nedostopni, določene funkcionalnosti pa morda ne bodo delovale pravilno.

Za spreminjanje nastavitev piškotkov sledite navodilom brskalnika (ki se običajno nahajajo pod zavihkom “Pomoč”, “Orodja” ali “Urejanje”) ali kliknite na spodnjo povezavo “Spreminjanje nastavitev piškotkov”.',
                'tabs' => 'all',
                'toolbar' => 'full',
                'media_upload' => 1,
                'delay' => 0,
            ),
            array(
                'key' => 'field_5af3f020fc6db',
                'label' => 'Button text continue browsing',
                'name' => 'button_text_continue_browsing',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Nadaljuj z ogledom strani',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af3f02bfc6dc',
                'label' => 'Button text cookie settings',
                'name' => 'button_text_cookie_settings',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Nastavitve piškotkov',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af3f03bfc6dd',
                'label' => 'Button style continue browsing',
                'name' => 'button_style_continue_browsing',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array(
                    'link' => 'link',
                    'button' => 'button',
                ),
                'default_value' => array(),
                'allow_null' => 0,
                'multiple' => 0,
                'ui' => 0,
                'ajax' => 0,
                'return_format' => 'value',
                'placeholder' => '',
            ),
            array(
                'key' => 'field_5af3f04cfc6de',
                'label' => 'Save settings button text',
                'name' => 'save_settings_button_text',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Potrdi izbiro piškotkov',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af3f05afc6df',
                'label' => 'Button accept cookies text (explicit consent)',
                'name' => 'button_accept_cookies_text_explicit_consent',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Sprejmem piškote',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af3f078fc6e0',
                'label' => 'Cookie settings button text (floating cookie)',
                'name' => 'cookie_settings_button_text_floating_cookie',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Nastavitve piškotkov',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5b1f5632b022f',
                'label' => 'Allow all cookies text',
                'name' => 'allow_all_cookies_text',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Dovoli vse piškotke',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5b1f564ab0230',
                'label' => 'Allow some cookies text',
                'name' => 'allow_some_cookies_text',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Dovoli nekatere piškotke',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5b1f565db0231',
                'label' => 'allow no cookies text',
                'name' => 'allow_no_cookies_text',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Ne dovoli nobenih piškotkov',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5b1f56bee3c9f',
                'label' => 'Choose cookies to allow text',
                'name' => 'choose_cookies_to_allow_text',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Izberite piškotke, ki jih želite dovoliti',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5b1f5688e3c9e',
                'label' => 'Continue browsing text',
                'name' => 'continue_browsing_text',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => 'Nadaljuj z ogledom strani',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-cookie-settings',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

    acf_add_local_field_group(array(
        'key' => 'group_5af2d45a1c5ab',
        'title' => 'GDPR settings',
        'fields' => array(
            array(
                'key' => 'field_5af2d435c2939',
                'label' => 'Enable cookies settings',
                'name' => 'enable_cookies_settings',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array(
                    'no' => 'no',
                    'yes' => 'yes',
                ),
                'default_value' => array(),
                'allow_null' => 0,
                'multiple' => 0,
                'ui' => 0,
                'ajax' => 0,
                'return_format' => 'value',
                'placeholder' => '',
            ),
            array(
                'key' => 'field_5af2d421c2938',
                'label' => 'Enable Privacy policy',
                'name' => 'enable_privacy_policy',
                'type' => 'select',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'choices' => array(
                    'no' => 'no',
                    'yes' => 'yes',
                ),
                'default_value' => array(),
                'allow_null' => 0,
                'multiple' => 0,
                'ui' => 0,
                'ajax' => 0,
                'return_format' => 'value',
                'placeholder' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'gdpr-settings',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));

    acf_add_local_field_group(array(
        'key' => 'group_5af4028e929b1',
        'title' => 'Privacy policy',
        'fields' => array(
            array(
                'key' => 'field_5af402c960984',
                'label' => 'Notice text for Privacy policy bar',
                'name' => 'ppnoticetext',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af4031860986',
                'label' => 'Privacy policy link text',
                'name' => 'privacy_link_text',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
            array(
                'key' => 'field_5af402e760985',
                'label' => 'Privacy policy page',
                'name' => 'pppage',
                'type' => 'post_object',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'post_type' => array(),
                'taxonomy' => array(),
                'allow_null' => 0,
                'multiple' => 0,
                'return_format' => 'object',
                'ui' => 1,
            ),
            array(
                'key' => 'field_5af402972dc42',
                'label' => 'Latest cookie name',
                'name' => 'pp_cookie_name',
                'type' => 'text',
                'instructions' => '',
                'required' => 0,
                'conditional_logic' => 0,
                'wrapper' => array(
                    'width' => '',
                    'class' => '',
                    'id' => '',
                ),
                'default_value' => '',
                'placeholder' => '',
                'prepend' => '',
                'append' => '',
                'maxlength' => '',
            ),
        ),
        'location' => array(
            array(
                array(
                    'param' => 'options_page',
                    'operator' => '==',
                    'value' => 'acf-options-privacy-policy',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => '',
        'active' => 1,
        'description' => '',
    ));


endif;


?>