<?php
/*
Plugin Name: OW GDPR plugin
Plugin URI: http://www.optiweb.com
Description: Plugin that adds GDRP privacy policy & cookies.
Author: Anže Svoljšak,  Optiweb d.o.o.
Version: 2.1
Author URI: http://www.optiweb.com **/

//require za datoteko z funkcijami za prikaz administracije & nalaganje skript na front endu
function gdpr_load_admin()
{
    //adding it later so ACF plugin is loaded before for sure
    if (function_exists('acf_add_local_field_group')):
        require 'admin/admin-board.php';
        require 'admin/call-at-the-start.php';
        if (get_field("enable_privacy_policy", "option") == "yes") {
            require 'admin/privacy_policy.php';
        }
    endif;


}

add_action('plugins_loaded', 'gdpr_load_admin');

function addScripts()
{
    wp_enqueue_style("ow_cookies_style", get_site_url() . '/wp-content/plugins/ow-gdpr/css/style.css');
}

add_action('wp_enqueue_scripts', 'addScripts');

