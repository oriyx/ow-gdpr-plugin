jQuery("#selectppolicy").change(function(){
	location.replace(jQuery(this).find("option:selected").attr("link"));
});

function getCookie(c_name){
	  var c_value = document.cookie;
	  var c_start = c_value.indexOf(" " + c_name + "=");
	  if (c_start == -1){
		c_start = c_value.indexOf(c_name + "=");
	  }
	  if (c_start == -1)  {
		c_value = null;
	  }
	  else{
		c_start = c_value.indexOf("=", c_start) + 1;
		var c_end = c_value.indexOf(";", c_start);
		if (c_end == -1){
		  c_end = c_value.length;
		}
		c_value = unescape(c_value.substring(c_start,c_end));
	  }
	  return c_value;
	}
	
	//function that checks if cookies are disabled in browser
	function checkCookie(){
		var cookieEnabled = navigator.cookieEnabled;
		if (!cookieEnabled){ 
			document.cookie = "testcookie";
			cookieEnabled = document.cookie.indexOf("testcookie")!=-1;
		}
		if(cookieEnabled){
			cookiesEnabled();
		}else{
			showCookieFail();
		}
	}
	
	function checkPPCookie(){
		var cookieppname = "testname";
		var exdate=new Date();
        exdate.setDate(exdate.getDate() + 365);
		jQuery.ajax({
					url: gdprajaxurl,
					method: 'post',
					data: {
						action: 'returncookiename',
					},
					success: function(cookieppname) {
						var ppCookie = getCookie(cookieppname);
						//console.log(cookieppname);

						if(ppCookie == false || ppCookie == null || ppCookie == undefined){
							
							jQuery.ajax({
								url: gdprajaxurl,
								method: 'post',
								data: {
									action: 'display_pp_notice',
								},
								success: function(result) {
									//console.log(result);
									jQuery(".privacynotice").html(result);
									jQuery(".ppolicylink").attr("forcookie", cookieppname);
									jQuery(".closeprivacy").attr("forcookie", cookieppname);
								},
								error: function(result) {
									console.log("Ajax error");
								}
							}); 
							jQuery("html").addClass("withprivacy");
						}
					},
					error: function(result) {
						console.log("Ajax error");
					}
				}); 
		
	}
	//jQuery(document).ready(function(){
	checkPPCookie();
	//});
	jQuery(document).ajaxComplete(function(){
	jQuery(".ppolicylink").click(function(){
		var cookieppname = jQuery(this).attr("forcookie");
		var exdate=new Date();
        exdate.setDate(exdate.getDate() + 365);
		document.cookie = cookieppname+"testname=true; expires="+exdate.toUTCString()+"; path=/";
		
	});
	jQuery(".closeprivacy").click(function(){
		var cookieppname = jQuery(this).attr("forcookie");
		var exdate=new Date();
        exdate.setDate(exdate.getDate() + 365);
		document.cookie = cookieppname+"=true; expires="+exdate.toUTCString()+"; path=/";
		location.reload();
		
	});
	});
	


