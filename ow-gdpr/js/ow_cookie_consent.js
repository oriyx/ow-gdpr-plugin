(function($) {
	//returns value of cookie
	var currentLang = document.documentElement.lang;
	//var currentLang = wpml_cookie;
	currentLang = currentLang.split('-', 1);
	currentLang = currentLang[0];
	//console.log(currentLang);
	function getCookie(c_name){
	  var c_value = document.cookie;
	  var c_start = c_value.indexOf(" " + c_name + "=");
	  if (c_start == -1){
		c_start = c_value.indexOf(c_name + "=");
	  }
	  if (c_start == -1)  {
		c_value = null;
	  }
	  else{
		c_start = c_value.indexOf("=", c_start) + 1;
		var c_end = c_value.indexOf(";", c_start);
		if (c_end == -1){
		  c_end = c_value.length;
		}
		c_value = unescape(c_value.substring(c_start,c_end));
	  }
	  return c_value;
	}
	
	//function that checks if cookies are disabled in browser
	function checkCookie(){
		var cookieEnabled = navigator.cookieEnabled;
		if (!cookieEnabled){ 
			document.cookie = "testcookie";
			cookieEnabled = document.cookie.indexOf("testcookie")!=-1;
		}
		if(cookieEnabled){
			cookiesEnabled();
		}else{
			showCookieFail();
		}
	}
	
	//if cookies are disabled, this happens
	function showCookieFail(){
        $.ajax({
            url: ajaxurl,
            method: 'post',
            data: {
                action: 'cookies_disabled_notice',
				gdprlanguage: currentLang,
            },
            success: function(result) {
                $("#ow_cookie_consent_container").html(result);
				//console.log('Result 2 - cookies_disabled_notice');
            },
			error: function(xhr, desc, err) {
				console.log(xhr);
				console.log("Details: " + desc + "\nError:" + err);
			}
        }); 
	}
	
	//if cookies are enabled, this happens
	function cookiesEnabled(){
		var exdate=new Date();
        exdate.setDate(exdate.getDate() + 7);
		var pCookie = getCookie("tracking_permission_displayed");
		var sCookie = getCookie("tracking_permission");
		// console.log(pCookie);
		//check if donottrack is enabled
		var isDNT = navigator.doNotTrack == "yes" || navigator.doNotTrack == "1" || navigator.msDoNotTrack == "1" || window.doNotTrack == "1";
		// console.log(isDNT);
		if(isDNT){ //dnt enabled
			if(pCookie == false || pCookie == null || pCookie == undefined){
				//dnt enabled and first load - display notice + settings
				 $.ajax({
					url: ajaxurl,
					method: 'post',
					data: {
						action: 'dnt_enabled_notice',
						gdprlanguage: currentLang,
					},
					success: function(result) {
						$("#ow_cookie_consent_container").html(result);
						//console.log('Result 2 - dnt_enabled_notice');
					},
					error: function(result) {
						console.log("Ajax error");
					}
				}); 
				
			}else{
				//dnt enabled and notice already displayed - display settings
				$.ajax({
					url: ajaxurl,
					method: 'post',
					data: {
						action: 'display_cookie_settings_button',
						gdprlanguage: currentLang,
					},
					success: function(result) {
						$("#ow_cookie_consent_container").html(result);
						//console.log('Result 2 - display_cookie_settings_button');
						//console.log(result);
					},
					error: function(result) {
						console.log("Ajax error");
					}
				}); 
			}
		}else{ //dnt disabled
			if(pCookie == false || pCookie == null || pCookie == undefined){
				//first load without DNT
				$.ajax({
					url: ajaxurl,
					method: 'post',
					data: {
						action: 'display_cookie_notice_regular',
						gdprlanguage: currentLang,
					},
					success: function(result) {
						$("#ow_cookie_consent_container").html(result);
						//console.log('Result 2 - display_cookie_notice_regular');
					},
					error: function(result) {
						console.log("Ajax error");
					}
				}); 
				// document.cookie = "tracking_permission=all; expires="+exdate.toUTCString()+"; path=/";
			}else{
				//consequent load without DNT
				if(sCookie == false || sCookie == null || sCookie == undefined){
					document.cookie = "tracking_permission=all; expires="+exdate.toUTCString()+"; path=/";
				}
				$.ajax({
					url: ajaxurl,
					method: 'post',
					data: {
						action: 'display_cookie_settings_button',
						gdprlanguage: currentLang,
					},
					success: function(result) {
						$("#ow_cookie_consent_container").html(result);
						//console.log('Result 2 - display_cookie_settings_button');
					},
					error: function(xhr, desc, err) {
						console.log(xhr);
						console.log("Details: " + desc + "\nError:" + err);
					}
				}); 
			}
			
		}
		
		if(pCookie == false || pCookie == null || pCookie == undefined){
			//document.cookie = "tracking_permission_displayed=true; expires="+exdate.toUTCString()+"; path=/";
		}else{
			function myFunction(item, index) {
				if(index == body){
					$("#trackingscripts").html(item);
				}
			}
			//load tracking scripts in the end, if cookie is set and is not none
			$.ajax({
				url: ajaxurl,
				method: 'post',
				dataType: 'json',
				data: {
					action: 'load_tracking_scripts',
					gdprlanguage: currentLang,
				},
				success: function(result) {
					//console.log(result);
					$("#trackingscripts").html(result['body']);
					//$("#trackingscripts").html(result);
					$(result.head).insertAfter('#trackingheadscripts');
				},
				error: function(result) {
					console.log("Ajax error");
				}
			});
		}

	}
	//trigget the whole thing on load
	$(document).ready(function(){
		checkCookie();

		$.ajax({
			url: ajaxurl,
			method: 'post',
			data: {
				action: 'load_secured_cookies',
				gdprlanguage: currentLang,
			},
			success: function(result) {
				window.secureCookieNameOW = result;
			},
			error: function(result) {
				console.log("Ajax error");
			}
		});


		// odkomentiraj če potrebuješ youtube predvajanje v ozadju
		/*var sCookie = getCookie("tracking_permission");
		// console.log(sCookie.indexOf("v"));
		if((sCookie == '' || sCookie == null || sCookie.indexOf("v") == -1) && sCookie != 'all' ){
			$("a[href*=youtube], a[href*=vimeo], a[href*=youtube]").click(function(event){
				event.preventDefault();
				event.stopPropagation()
				console.log($(this).attr("href"));
					$(".linkforvideopopup").attr("href", $(this).attr("href"));
				setTimeout(function(){ 
					$(".avia-popup").remove();
					$(".youtubepopup").removeClass("hidden");
					
					$(".avia-popup").remove();
				}, 500);
			});
			
			if($(".av-section-video-bg.avia-slide-slider").length){
				$(".av-section-video-bg.avia-slide-slider").each(function(){
					if($(this).html().indexOf("youtube") != -1 || $(this).html().indexOf("vimeo") != -1){
						bgurl = $(this).closest(".avia-section").attr("style").split(";")[2];
						if(bgurl.indexOf("wp-content") == -1){
							bgurl = $(this).closest(".avia-section").attr("style").split(";")[1];
						}
						$(this).html('<div class="bgreplace" style="'+bgurl+'"></div>');
					}
				});				
			}
			
			setTimeout(function(){ 
				$("a[href*=youtube], a[href*=vimeo], a[href*=youtube]").each(function(event){
					$(this).removeClass("lightbox-added");
					$(this).removeClass("mfp-iframe");
				});

			}, 500);
			$(".closeerrorvideo").click(function(){
				$(".youtubepopup").addClass("hidden");
			});
		} */
		
	});
	$(document).ajaxComplete(function(){

		//open about cookies popup
		$(".cookies_container p a, a[href='http://aboutcookies']").click(function(event){
			event.preventDefault();
			if($(".popupcookiesabout").length){
				$(".bottomfooter").addClass("hidenotice");
				$(".popupcookiessettings").addClass("hidenotice");
				$(".popupcookiesabout").removeClass("hidenotice");
				$(".opencookiesettings ").removeClass("hiddenbutton");
			}
		});
		//open cookie settings on button click
		$(".opencookiesettings, .cookiesettingsfooter, a[href='http://cookiesettings']").click(function(event){
			event.preventDefault();
				$(".popupcookiessettings").removeClass("hidenotice");
				$(".popupcookiesabout").addClass("hidenotice");
				$(".popupcookies").addClass("hidenotice");
				$(".bottomfooter").addClass("hidenotice");
				$(".youtubepopup").addClass("hidden");
		});
		
		$(".popupcookiesabout .closecpopup ").click(function(){
				$(".popupcookiesabout").addClass("hidenotice");
		});
		
		//open cookie settings on button click
		$(".acceptcookies, .forcereload").click(function(){
			var exdate=new Date();
			exdate.setDate(exdate.getDate() + 7);
			var wholeCookieName = [];
			var wholeCookieValue = [];

			$("input.ow-custom-cookie-input").each(function(){
				wholeCookieName.push($(this).attr("name"));
				wholeCookieValue.push($(this).val());
			});

			/* CUSTOM Cookie Part - Izdelal Peter v primeru da kaj break-a */
			for(var i = 0; i < wholeCookieName.length; i++){
				if(secureCookieNameOW == false || secureCookieNameOW == null || secureCookieNameOW == undefined){
					document.cookie = wholeCookieName[i]+"="+wholeCookieValue[i]+"; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/";
				}else{
					document.cookie = wholeCookieName[i]+"="+wholeCookieValue[i]+"; expires="+exdate.toUTCString()+"; path=/";
				}
			}
			/* End - CUSTOM Cookie Part */

			if(secureCookieNameOW == false || secureCookieNameOW == null || secureCookieNameOW == undefined){
				document.cookie = "tracking_permission=all; expires="+exdate.toUTCString()+"; path=/;";
				document.cookie = "tracking_permission_displayed=true; expires="+exdate.toUTCString()+"; path=/;";
			}else{
				document.cookie = "tracking_permission=all; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/;";
				document.cookie = "tracking_permission_displayed=true; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/;";
			}
		});
		
		//open cookie settings on button click
		$(".dontacceptcookies").click(function(){
			var exdate=new Date();
			exdate.setDate(exdate.getDate() + 7);
			var wholeCookieName = [];
			var wholeCookieValue = [];
			$("input.ow-custom-cookie-input").each(function(){
				wholeCookieName.push($(this).attr("name"));
				wholeCookieValue.push($(this).val());
			});

			/* CUSTOM Cookie Part - Izdelal Peter v primeru da kaj break-a */
			for(var i = 0; i < wholeCookieName.length; i++){
				if(secureCookieNameOW == false || secureCookieNameOW == null || secureCookieNameOW == undefined){
					document.cookie = wholeCookieName[i]+"=false; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/";
				}else{
					document.cookie = wholeCookieName[i]+"="+wholeCookieValue[i]+"; expires="+exdate.toUTCString()+"; path=/";
				}
			}
			/* End - CUSTOM Cookie Part */

			if(secureCookieNameOW == false || secureCookieNameOW == null || secureCookieNameOW == undefined){
				document.cookie = "tracking_permission=none; expires="+exdate.toUTCString()+"; path=/;";
				document.cookie = "tracking_permission_displayed=true; expires="+exdate.toUTCString()+"; path=/;";
			}else{
				document.cookie = "tracking_permission=none; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/;";
				document.cookie = "tracking_permission_displayed=true; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/;";
			}
		});
		//show or hide cookies to choose
		$(".csettingsradio, .csettingscb, .closecsettingstext").click(function(){
			var exdate=new Date();
			exdate.setDate(exdate.getDate() + 7);
			var checkedCookieName = [];
			var checkedCookieValue = [];
			var wholeCookieName = [];
			var wholeCookieValue = [];


			$("input.ow-custom-cookie-input:checked").each(function(){
				checkedCookieName.push($(this).attr("name"));
				checkedCookieValue.push($(this).val());
			});
			$("input.ow-custom-cookie-input").each(function(){
				wholeCookieName.push($(this).attr("name"));
				wholeCookieValue.push($(this).val());
			});
			/* console.log(wholeCookieName);
			console.log(checkedCookieName); */

			/* CUSTOM Cookie Part - Izdelal Peter v primeru da kaj break-a */
			for(var i = 0; i < wholeCookieName.length; i++){

				var currName = checkedCookieName[i];
				var currValue = checkedCookieValue[i];
				var doesItInclude = checkedCookieName.includes(wholeCookieName[i]);

				if(secureCookieNameOW == false || secureCookieNameOW == null || secureCookieNameOW == undefined){
					if(doesItInclude){
						document.cookie = wholeCookieName[i]+"="+wholeCookieValue[i]+"; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/";
						document.cookie = "tracking_permission_displayed=true; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/";
					}else{
						document.cookie = wholeCookieName[i]+"=false; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/";
						document.cookie = "tracking_permission_displayed=true; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/";
					}
				}else{
					if(doesItInclude){
						document.cookie = wholeCookieName[i]+"="+wholeCookieValue[i]+"; expires="+exdate.toUTCString()+"; path=/";
						document.cookie = "tracking_permission_displayed=true; expires="+exdate.toUTCString()+"; path=/";
					}else{
						document.cookie = wholeCookieName[i]+"=false; expires="+exdate.toUTCString()+"; path=/";
						document.cookie = "tracking_permission_displayed=true; expires="+exdate.toUTCString()+"; path=/";
					}
				}
			}
			/* End - CUSTOM Cookie Part */


			if($("input[name='generalcookiesettings']:checked").val() == 'justsome'){
				$(".justsomecookies").addClass("show");
			}else{
				$(".justsomecookies").removeClass("show");
			}
			if($("input[name='generalcookiesettings']:checked").val() != 'justsome'){
				var decision = $("input[name='generalcookiesettings']:checked").val();
				//console.log(decision);
				if(secureCookieNameOW == false || secureCookieNameOW == null || secureCookieNameOW == undefined){
					for(var i = 0; i < wholeCookieName.length; i++){
						if(decision == 'all'){
							document.cookie = wholeCookieName[i]+"="+wholeCookieValue[i]+"; expires="+exdate.toUTCString()+"; path=/";
						}else{
							document.cookie = wholeCookieName[i]+"=false; expires="+exdate.toUTCString()+"; path=/";
						}
					}
					document.cookie = "tracking_permission="+$("input[name='generalcookiesettings']:checked").val()+"; expires="+exdate.toUTCString()+"; path=/";
					document.cookie = "tracking_permission_displayed=true; expires="+exdate.toUTCString()+"; path=/";
				}else{
					for(var i = 0; i < wholeCookieName.length; i++){
						if(decision == 'all'){
							document.cookie = wholeCookieName[i]+"="+wholeCookieValue[i]+"; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/";
						}else{
							document.cookie = wholeCookieName[i]+"=false; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/";
						}
					}
					document.cookie = "tracking_permission="+$("input[name='generalcookiesettings']:checked").val()+"; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/";
					document.cookie = "tracking_permission_displayed=true; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/";
				}
			}else{
				var cookietoset = "scripts|";

				$("input[name='selectedcookies']:checked").each(function(){
					cookietoset += $(this).val() + '|';
				});
				if(secureCookieNameOW == false || secureCookieNameOW == null || secureCookieNameOW == undefined){
					document.cookie = "tracking_permission="+cookietoset+"; expires="+exdate.toUTCString()+"; path=/";
					document.cookie = "tracking_permission_displayed=true; expires="+exdate.toUTCString()+"; path=/";
				}else{
					document.cookie = "tracking_permission="+cookietoset+"; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/";
					document.cookie = "tracking_permission_displayed=true; expires="+exdate.toUTCString()+";"+secureCookieNameOW+" path=/";
				}
			}
		});
		/*$(".popupcookiessettings, .popupcookiesabout").click(function(event){
			$(this).addClass("hidenotice");
		});*/
		$(".innerpopupcookies").click(function(event){
		  event.stopPropagation();
		});

		//hide popups/bottom bars and display settings button
		/*$(".closenotice, .popuplinks > div, .dontacceptcookies, .closecsettingstext ").click(function(){
			if($(".popupcookies").length){
				$(".popupcookies").addClass("hidenotice");
				$(".popupcookiessettings").addClass("hidenotice");
				$(".csettingsbutton").removeClass("hiddenbutton");
			}else{
				$(".bottomfooter").addClass("hidenotice");
				$(".popupcookiessettings").addClass("hidenotice");
				$(".csettingsbutton").removeClass("hiddenbutton");
			}
			if($(".popupcookiesabout:not(.hidenotice)").length){
				$(".popupcookiesabout").addClass("hidenotice");
			}
		});*/
		$(".closecsettingstext").click(function(){
			location.reload();
		});
		$(".forcereload").click(function(){
			location.reload();
		});
		$(".bottomfooter .acceptcookies").click(function(){
			location.reload();
		});
		
	});


	
})( jQuery );